import argparse
import json
import os


def install(manager, packages):
    # works for pamac and snap
    for package in packages:
        os.system(manager + ' install ' + package)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='install packages via snap and pacman')
    parser.add_argument('files', nargs='*', help='JSON files containing package names')
    parser.print_help()
    args = parser.parse_args()
    for filename in args.files:
        try:
            file = json.loads(open(filename, 'r').read())
            for man, paks in file.items():
                install(man, paks)
        except FileNotFoundError:
            pass

